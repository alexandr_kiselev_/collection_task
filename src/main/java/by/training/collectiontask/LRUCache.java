package by.training.collectiontask;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * LRUCache realisation.
 *
 * @param <K>
 * @param <V>
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {

    /**
     * Count maximum entries.
     */
    private final int maxEntries;

    /**
     * LRUCache constructor.
     *
     * @param maxEntries
     */
    LRUCache(final int maxEntries) {
        super(maxEntries + 1, 1.0f, true);
        this.maxEntries = maxEntries;
    }

    /**
     * Return true for remove eldest entry if no more free space.
     *
     * @param eldest
     * @return
     */
    @Override

    protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
        return super.size() > maxEntries;
    }

    /**
     * Return string with all cache values.
     *
     * @return
     */
    @Override
    public java.lang.String toString() {
        java.lang.String returner = "";

        for (Map.Entry<K, V> me : this.entrySet()) {
            returner = returner + " " + me.getValue();
        }

        return returner;
    }
}
